# frozen_string_literal: true

module Resolvers
  module Geo
    class MergeRequestDiffRegistriesResolver < BaseResolver
      include RegistriesResolver
    end
  end
end
